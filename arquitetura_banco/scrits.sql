CREATE TABLE `crud_database`.`usuarios` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`nome` VARCHAR(100) NOT NULL ,
	`cpf` VARCHAR(14) NULL ,
	`data_nasc` DATE NULL , 
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;

alter table usuarios
add column deleted boolean;

alter table usuarios
add column email VARCHAR(50);