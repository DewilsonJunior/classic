<!DOCTYPE html>

<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<title>CRUD</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/common.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>

<body>
	<div class="container">
		<div class="box">	
			<div class="home">	
				<div class="form-pesquisar">
					<div class="input-pesquisar">
						<input type="text" id="input-pesquisar" placeholder="Ex:.João" />
					</div>

					<div class="btn-group">
						<a href="cadastro_usuario.php" class="btn btn-novo">Novo</a>
						
						<button class="btn btn-pesquisar" id="btn-pesquisar">Pesquisar</button>
					</div>
				</div>

				<div class="tabela-pessoas">
					<table id="tabela-pessoas">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nome</th>
								<th>CPF</th>
								<th>Data de Nascimento</th>
								<th>Email</th>
								<th>Opções</th>
							</tr>
						</thead>
					<tbody>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="js/index.js"></script>
</body>

</html>