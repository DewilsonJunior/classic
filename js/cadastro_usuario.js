//==========================	validação e estilização do formulario	=======================================
function mascara(objeto,funcao){
	v_obj=objeto;
	v_fun=funcao;
	setTimeout("execmascara()",1);
}

function execmascara(){
	v_obj.value = v_fun(v_obj.value);
}

function mtel(valor_input){

	valor_input = valor_input.replace(/^(\d{3})(\d{3})(\d{3})(\d{2})/g,"\$1.\$2.\$3\-\$4");
	
	return valor_input;
}

window.onload = function(){
	document.getElementById("input_cpf").onkeypress = function(){
		mascara(this, mtel);
	}
}

//===============================		Processo de criação dos dados	=======================================

var btnSalvar = document.getElementById("btn_salvar");

btnSalvar.onclick = function(event){
	
	event.preventDefault();
	
	var nome = document.getElementById("input_nome").value;
	var cpf = document.getElementById("input_cpf").value;
	var dataNasc = document.getElementById("input_dataNasc").value;
	var email = document.getElementById("input_email").value;

	if(!nome){
		alert("por favor preencha o nome");
		document.getElementById("input_nome").focus();
	}
	else if(!cpf){
		alert("por favor preencha o CPF");
		document.getElementById("input_cpf").focus();
	}
	else{
		var result = persistirDados(nome, cpf, dataNasc, email);
	}
}

function persistirDados(nome, cpf, dataNasc, email){
	
	var data = {};

	data = {
		nome: nome,
		cpf: cpf,
		dataNascimento: dataNasc,
		email: email
	};
	
	data = JSON.stringify(data);
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(this.readyState == 4 && this.status == 200){
			var callback = this.responseText;
			
			if(callback == "usuario_existente"){
				alert("Já existe usuário cadastrado com esse CPF.");
			}
			else if(!callback){
				alert("Não foi possivel salvar o usuário, por favor entre em contato com o suporte.");
			}
			else{
				alert("Usuário cadastrado com sucesso!");
				window.location = "/classic/index.php";
			}
		}
	};
	xhttp.open("GET", "cadastro_usuario_func.php?data="+data, true);
	xhttp.send();
	
	return "ok";
	
}