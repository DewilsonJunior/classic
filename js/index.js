/*inicio do evento ready com js puro*/
function ready(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            var callback = this.responseText;

            if(!!callback){
                mostrarDados(callback);

            }
            
        }
    };
    xhttp.open("GET","index_func.php?option=home", true);
    xhttp.send();
}

function completed(){
    document.removeEventListener("DOMContentLoaded", completed);
    window.removeEventListener("load", completed);
    ready();
}

if(document.readyState === "complete" || document.readyState !== "loading"){
    ready();
}
else{
    document.addEventListener("DOMContentLoaded", completed);
    window.addEventListener("load", completed);
} 
/*fim do evento ready com js puro*/

var btnPesquisar = document.getElementById("btn-pesquisar");

btnPesquisar.onclick = function(event){

    event.preventDefault();

    var nome = document.getElementById("input-pesquisar").value;

    if(!!nome){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){
                var callback = JSON.parse(this.responseText);
    
                if(!!callback){
                    
                    limpaTabela();
                    mostrarDados(callback);
                }
            }
        };
        xhttp.open("GET","index_func.php?option=pesquisar&nome="+nome, true);
        xhttp.send();
    }
    else{
        alert("Digite um nome primeiro");
        document.getElementById("input-pesquisar").focus();
    }

}


/*===========================   Sessão de funções   ===================================*/

function mostrarDados(callback){

    let dados = JSON.parse(callback);

    var tbody = document.getElementById("tabela-pessoas").children[1];

    for(var i in dados){

        console.log(dados[i]);

        var linha = document.createElement("tr");

        var tdId = document.createElement("td");
        var tdNome = document.createElement("td");
        var tdCpf = document.createElement("td");
        var tdDataNasc = document.createElement("td");
        var tdEmail = document.createElement("td");
        var tdEmail = document.createElement("td");
        var btnAtualizar = document.createElement('a');
        var btnExcluir = document.createElement('button');

        var bodyId = document.createTextNode(dados[i]["id"]);
        var bodyNome = document.createTextNode(dados[i]["nome"]);
        var bodyCpf = document.createTextNode(formatarCpf(dados[i]["cpf"]));
        var bodyDataNasc = document.createTextNode(dados[i]["data_nasc"]);
        var bodyEmail = document.createTextNode(dados[i]["email"]);
        var bodyBtnAtualizar = document.createElement("img");
        var bodyBtnExcluir = document.createElement("img");

        bodyBtnAtualizar.src = "img/outline_mode_edit_black_24dp.png";
        bodyBtnExcluir.src = "img/outline_delete_outline_black_24dp.png";

        tdId.appendChild(bodyId);
        tdNome.appendChild(bodyNome);
        tdCpf.appendChild(bodyCpf)
        tdDataNasc.appendChild(bodyDataNasc);
        tdEmail.appendChild(bodyEmail);
        btnAtualizar.appendChild(bodyBtnAtualizar);
        btnExcluir.appendChild(bodyBtnExcluir);

        btnAtualizar.id = "btn_atualizar";
        btnAtualizar.classList.add("btn");
        btnAtualizar.classList.add("btn-icon");
        btnAtualizar.setAttribute("href","atualiza_usuario.php?id=" + callback[i]["id"]);
        
        btnExcluir.id = "btn_excluir";
        btnExcluir.classList.add("btn");
        btnExcluir.classList.add("btn-icon");
        btnExcluir.addEventListener("click", excluir);

        linha.appendChild(tdId);
        linha.appendChild(tdNome);
        linha.appendChild(tdCpf);
        linha.appendChild(tdDataNasc);
        linha.appendChild(tdEmail);
        linha.appendChild(btnAtualizar);
        linha.appendChild(btnExcluir);

        tbody.appendChild(linha);
    }
}

function formatarCpf(cpf){

    console.log(cpf);
    //cpf = cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,"\$1.\$2.\$3\-\$4");
    
   //return cpf;
}

function atualizarDados(){
    var id = this.parentNode.children[0].innerText;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState && this.status == 200){
            var callback = JSON.parse(this.responseText);

            console.log(callback);
        }
    };
    xhttp.open("GET", "index_func.php?option=atualizar&id=" + id, true);
    xhttp.send();
}

function limpaTabela(){

    var tabela = document.getElementById("tabela-pessoas").children[1];

    while(tabela.firstChild){
        tabela.removeChild(tabela.firstChild);
    }
}

function excluir(){

    console.log(this.parentNode);

    if(confirm("Deseja realmente excluir o seguinte usuário?")){
        var id = this.parentNode.firstChild.innerText;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){
                var callback = this.responseText;
    
                if(!callback){
                    alert("Não foi possivel salvar o usuário, por favor entre em contato com o suporte.");
                }
                else{
                    alert("Usuário excluido com sucesso!");
                    location.reload();
                }
            }
        };
        xhttp.open("GET","index_func.php?option=excluir&id="+id, true);
        xhttp.send();
    }
}