/*inicio da função ready com js puro*/
function ready(){
    var queryString = location.search.slice(1);

    var id = queryString.replace("id=","");

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState && this.status == 200){

            if(!!this.responseText){
                var callback = JSON.parse(this.responseText);
                estruturaFormulario(callback);
            }
        }
    };
    xhttp.open("GET", "atualiza_usuario_func.php?option=home&id=" + id, true);
    xhttp.send();
}

function completed(){
    document.removeEventListener("DOMContentLoaded", completed);
    document.removeEventListener("load", completed);
    ready();
}

if(document.readyState === "complete" || document.readyState !== "loading"){
    ready();
}
else{
    document.addEventListener("DOMContentLoaded", completed);
    document.addEventListener("load", completed);
}
/*fim da função ready com js puro*/

var btnSalvar = document.getElementById("btn_salvar");

btnSalvar.onclick = function(event){

    event.preventDefault();

    var id = location.search.slice(1).replace("id=","");
    var nome = document.getElementById("input_nome").value;
    var cpf = document.getElementById("input_cpf").value;
    var dataNasc = document.getElementById("input_dataNasc").value;
    var email = document.getElementById("input_email").value;

    var result = persistirDados(id, nome, cpf, dataNasc, email);

}

function estruturaFormulario(data){
    
    document.getElementById("input_nome").value = data[0]["nome"];
    document.getElementById("input_cpf").value = data[0]["cpf"];
    document.getElementById("input_dataNasc").value = data[0]["data_nasc"];
    document.getElementById("input_email").value = data[0]["email"];
}

function persistirDados(id, nome, cpf, dataNasc, email){
    var data = [];

    data.push("id: " + id);
    data.push("nome: " + nome);
    data.push("cpf: " + cpf);
    data.push("dataNasc: " + dataNasc);
    data.push("email: " + email);

    data = JSON.stringify(data);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            var callback = JSON.parse(this.responseText);

            if(callback == "erro_ao_atualizar"){
                alert("Ocorreu um erro ao tentar atualizar, por favor entre em contato com o suporte.");
            }
            else{
                alert("Usuário atualizado com sucesso!");
                window.location = "/classic/index.html";
            }

        }
    };
    xhttp.open("GET","atualiza_usuario_func.php?option=atualizar&data="+data, true);
    xhttp.send();
}