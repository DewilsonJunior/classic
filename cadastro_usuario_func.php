<?php
include("include/SqlScripts.php");
	
	if(!empty($_GET["data"])){
		$data = $_GET["data"];

		$usuario = array();
		
		$data = json_decode($data);
		
		$usuario["nome"] =$data->nome;
		$usuario["cpf"] = $data->cpf;
		$usuario["dataNasc"] =  $data->dataNascimento;
		$usuario["email"] =  $data->email;

	}
	
	if(!empty($usuario["cpf"])){

		$usuario["cpf"] = str_replace(array(".","-"), "", $usuario["cpf"]);
		
		$campos = ["id","cpf"];
		$filtroBusca = " where cpf = '".$usuario["cpf"]."'";
		
		$newUsuario["duplicata"] = SqlScripts::buscar($campos, $filtroBusca);	

		if(empty($newUsuario["duplicata"])){
			
			$result = SqlScripts::criar($usuario);

			if($result){
			
				echo json_encode("cadastrado_com_sucesso.");
			}
			else{
				$newUsuario["error"] = $result;
			}
		}
		else{
			echo json_encode("usuario_existente");
			exit();
		}
		
	}
?>