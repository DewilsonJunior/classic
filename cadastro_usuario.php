<!DOCTYPE html>

<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<title>CRUD</title>
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<link rel="stylesheet" type="text/css" href="css/responsividade.css">
</head>

<body id="layout">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="box">			
					<div class="row">
						<div class="col-12">
							<div class="titulo_cadastro_usuario">
								<h1>Cadastro de Usuário</h1>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12">
							<form class="form_cadastro_usuario" action="#" method="get">
								<label for="input_nome">Nome</label>	
								<input type="text" id="input_nome" name="input_nome" required/>
								
								<label for="input_cpf">CPF</label>
								<input type="text" id="input_cpf" name="input_cpf" maxlength="11" required/>
								
								<label for="input_dataNasc">Data de Nascimento</label>
								<input type="date" id="input_dataNasc" name="input_dataNasc" />
								
								<label for="input_email">Email</label>
								<input type="email" id="input_email" name="input_email" />
								
								<input type="submit" id="btn_salvar" value="Salvar" />
								<a href="index.html">Voltar</a>
							</form>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="js/cadastro_usuario.js"></script>
</body>

</html>