<?php
include("include/conexao.php");

Class SqlScripts{
    
    function buscar($cols, $clausulaBusca){	

        $query = 	"select ". implode(',', $cols).
                    " from usuarios
                    ".$clausulaBusca;
        
        $result = mysqli_query($GLOBALS["SQL"], $query);

        if(!empty($result->num_rows)){
    
            while($rows = mysqli_fetch_assoc($result)){
                $data[] = $rows;
            }
    
            return $data;
        }
        else
            return 0;
    }
    
    function criar($data){
        
        $query = "	insert into usuarios
                        (nome, cpf, data_nasc, email)
                    values
                        ('".implode("','",$data)."')";
    
        $result = mysqli_query($GLOBALS["SQL"], $query);
        
        return $result;
    
    }

    /*$data = É um array com chave e valor, a função necessita de um parametro com essas expecificações
    * Ex: $data([chave] => valor)
    */
    function atualizar($data){

        $campos = [];
        $id = $data["id"];

        unset($data["id"]);

        foreach($data as $key => $value){
            array_push($campos, $key."='".$value."'");
        }

        $query = "  update usuarios
                    set ". implode($campos,",") .
                    "where id = '".$id."'";

        $result = mysqli_query($GLOBALS["SQL"], $query);

        return $result;
    }

    function excluir($id){

        $query = "  delete from
                    usuarios
                    where id = '".$id."'";

        $result = mysqli_query($GLOBALS["SQL"], $query);

        return $result;
    }
}